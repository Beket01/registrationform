<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<title></title>
</head>
<body>
	<header>
		<div class="row my-3">
			<div class="col-md-8"></div>
			<div class="col-md-3">
				
				<form method="GET"> 
        <input type="submit" class="btn btn-lg btn-warning btn-block mt-4 mr-2" name="button1"
                value="Login"/> 
          
        <input type="submit" class="btn btn-lg btn-dark btn-block mt-4" name="button2"
                value="Sign in"/> 
    </form>
			</div>
			<div class="col-md-1"></div>
		</div>
	</header>

	<?php 
	require 'connect.php';
	?>	
	<?php
        if(isset($_GET['button1'])) { 
            include 'login.php';
        } 
        if(isset($_GET['button2'])) { 
            include 'signin.php';
        } 
    ?> 
</body>
</html>