<?php  
	if (isset($_POST['username']) && isset($_POST['password'])) {
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$query = "INSERT INTO users (username,email,password) values ('".$username."','".$email."','".$password."')";
		if ( !preg_match('/^[A-Za-z0-9]{1,31}$/', $username) ) {		
  			$usernameErr = "Неверный формат: username";
		}
		if(empty($usernameErr)){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  			$emailErr = "Неверный формат: email";
		}}

		if(empty($emailErr) && empty($usernameErr)){

		$uppercase = preg_match('#[A-Z]#', $password);
		$lowercase = preg_match('#[a-z]#', $password);
		$number    = preg_match('#[0-9]#', $password);
		$length    = strlen($password) >= 8;

		if(!$uppercase || !$lowercase || !$number || !$length) {
			$passwordErr = 'Плохая пароль';}
		}

		if(empty($usernameErr) && empty($emailErr) && empty($passwordErr)){
			if ($conn->query($query) === TRUE) {
			    $smsg = "Регистрация прошла успешно";
			} else {
			    $fsmsg = "Регистрация не прошла успешно";
			}
		}
		}
?>

	<div class="container">
		<form class="form-signin card" method="POST">
			<h2>Registration</h2>
			<?php if(isset($smsg)) { ?> <div class="alert alert-success" role="alert"><?php echo $smsg; ?> </div><?php } ?>
			<?php if(isset($fsmsg)) { ?> <div class="alert alert-danger" role="alert"><?php echo $fsmsg; ?> </div><?php } ?>

			<?php echo $usernameErr ?>
			<input type="text" name="username" class="form-control mt-2" placeholder="Username" required="">
			<?php echo $emailErr ?>
			<input type="email" name="email" class="form-control mt-2" placeholder="Email" required="">
			<?php echo $passwordErr ?>
			<input type="password" name="password" class="form-control mt-2" placeholder="Password" required="">
			<button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Register</button> 	
		</form>
	</div> 