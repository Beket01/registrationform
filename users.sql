-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
-- База данных: `practice`

-- Структура таблицы `users`

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `users` (`id`, `username`, `email`, `password`, `active`) VALUES
(5, 'Beket', 'Goalkiop@gmail.com', 'Xm09ddd02', NULL),
(6, 'Arman', 'Arman@gmail.com', 'Fmqqdg02', NULL),
(7, 'Maksat', 'Maks@gmail.com', 'Maks010102', NULL);


